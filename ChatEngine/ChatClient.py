'''
Created on Oct 8, 2012

@author: el-doc
'''

#import sys
#sys.stderr = open("some path")

import os
import wx
from thread import *
import socket
from collections import deque
import time

def encrypt(msg):
    key = 'mY_sEcReT_kEy'
    encryped = []
    for i, c in enumerate(msg):
        key_c = ord(key[i % len(key)])
        msg_c = ord(c)
        encryped.append(chr((msg_c + key_c) % 127))
    return ''.join(encryped)

def decrypt(encryped):
    key = 'mY_sEcReT_kEy'
    msg = []
    for i, c in enumerate(encryped):
        key_c = ord(key[i % len(key)])
        enc_c = ord(c)
        msg.append(chr((enc_c - key_c) % 127))
    return ''.join(msg)


class Client:
    
    username = None
    clientID = None
    
    def __init__ (self, name, ID):
        self.username = name
        self.clientID = ID
        

class ChatRoom:
    
    roomName = None
    roomID = None
    
    def __init__(self, name, ID):
        self.roomName = name
        self.roomID = ID
        


class ChatClient:
    
    serverSocket=None #socket for connection with server
    guiSingleton=None #frame kept for GUI communication
    clientsList = []
    chatRoomsList = []
    processQueue=None	# process queue for handling clients requests
    server = 1000
    lobby = 100
    userClient = None
    userCR = None
    serverIP=''

    def main(self,ip,username):
	self.processQueue=deque([])
	self.serverIP = ip
	
        # Connect to the server with the given ip
        try:
	    start_new_thread(self.dataProcessor,())
	    
	    self.serverSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	    address = (ip, 4321)
	    self.serverSocket.connect(address)
	    self.connect_to_server(ip,username)
	    #call success method after connecting on the main UI thread
	    #wx.CallAfter(self.guiSingleton.connectedToServer,ip)
	except socket.error, e:
	    #wx.CallAfter(self.guiSingleton.showError, e)
	    pass
	except:
	    pass
	#on a new thread, listen to messages send by the server on same opened connection
	start_new_thread(self.listenToServer,(self.serverSocket,))
    
    def listenToServer(self,(conn)):
	try:
	    while True:
		data = conn.recv(4096)
		data = decrypt(data)
		self.processQueue.append(data)
		if not data:
		    break
		#update UI with message received from the server on the main UI thread
		#wx.CallAfter(self.guiSingleton.recieved, data)
	except:
	    pass
	#close connection if broke due to error in recieved data
	conn.close()
    
    def listenToFileTransfers(self):
	port = 8000 + self.userClient.clientID
	address = ('localhost', port)
        filesSocket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        #filesSocket = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
        filesSocket.bind(address)
        filesSocket.listen(100)
        while (True):
            conn,clientAddress = filesSocket.accept()
            start_new_thread(self.recieveFileData,(conn,)) 
	filesSocket.close()

    def recieveFileData(self, (conn)):
	data = conn.recv(1024)
	data = decrypt(data)
	senderID, sep, rest = data.partition('%')
	recieverID, sep, rest = rest.partition('%')
	fileName, sep, rest = rest.partition('%')
	fileSize, sep, rest = rest.partition('%')
	
	if(self.userClient.clientID==int(recieverID)):
	    wx.CallAfter(self.guiSingleton.recieveFileRequest, self.clientForClientID(senderID)[1], fileName, fileSize, conn)

    def recieveFile(self, conn, path):
	if not path:
	    conn.close()
	    return
	f = open (path,'w')
	fileString=''
        while True:
	    data = conn.recv(1024)
            if not data:
		break
            fileString+=data
	conn.close()
	f.write(decrypt(fileString))
	f.close()
	wx.CallAfter(self.guiSingleton.fileRecieved, path)
	
    def dataProcessor(self):
	while True:
	    if self.processQueue:
		data = self.processQueue.popleft()
		print data
		if not data:
		    continue
		x,sep,z = data.partition('$')
		CMD, sep, MSGRest = z.partition('$')
		MSG=''
		if MSGRest[-1]=='#' and MSGRest[-2]=='$':
		    MSG = MSGRest[0:-2]
		else:
		    continue
		self.analyzeCMD(CMD, MSG)
	    else:
		time.sleep(1)
	return

    def analyzeCMD(self,CMD, MSG):
	cmdName,sep,rest = CMD.partition('%')
	if cmdName == 'CONN_FAILED':
            #tell GUI to return to main screen
            pass
	elif cmdName == 'USER_CONF':
            name,sep,ID = MSG.partition('%')
            if not self.userClient:
		start_new_thread(self.listenToFileTransfers,())
            self.userClient = Client(name,int(ID))
        elif cmdName =='CRS':
            self.request_rooms(MSG)
            #roomID, sep, rest = rest.partition('$')
	elif cmdName=='CR_CLIENTS':
            if int(rest) == self.userCR.roomID:
                self.request_clients(MSG)
        elif cmdName=='CR_MSG':
	    roomID, sep, rest = rest.partition('%')
            clientID, sep, rest = rest.partition('%')
            self.msgRecieved(clientID, roomID, MSG)
        elif cmdName == 'DEL_CR':
            roomID , sep, rest = rest.partition('%')
	    self.deleteChatRoom(roomID)
        elif cmdName=='NEW_ROOM':
            self.send_chatrooms_request()
        elif cmdName == 'INVITE':
            senderID, sep, roomID = rest.partition('%')
            #invite gui
        elif cmdName == 'ADD_SUCC':
            name,sep,ID = MSG.partition('%')
            self.userCR = ChatRoom(name,int(ID))
            if int(ID)==self.lobby:
		wx.CallAfter(self.guiSingleton.transferToLobby,)
            else:
                wx.CallAfter(self.guiSingleton.transferToCR,int(ID))
	elif cmdName == 'KICKED':
		ID,sep,rest = rest.partition('%')
		self.userCR = ChatRoom('lobby',self.lobby)
		wx.CallAfter(self.guiSingleton.transferToLobby,)
            #transfer to room in GUI
        elif cmdName == 'RECONN_SUCC':
            pass
	elif cmdName == 'ADD_CLIENT':
	    roomID, sep, rest = rest.partition('%')
            clientID, sep, usrn = MSG.partition('<!~`~!>')
            if int(roomID) != int(self.userCR.roomID):
                return
            if int(clientID) != self.userClient.clientID:
		self.clientsList.append((int(clientID),usrn))
		wx.CallAfter(self.guiSingleton.updateClientsList, self.clientsList)
	else:
	    print 'error in CMD: ' , CMD ,' ,msg: ', MSG
    
    def send_file(self,path,reciever,isUDP):
        filename=''
        x = -1
        while (path[x]!="/"):
            x-=1
        else:
            x+=1
            filename = path[x:]
        f = open (path,'rb')
        s = None
        if isUDP:
	    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        else:
	    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((self.serverIP, 8888))
        
        time.sleep(2.0)
        
        udp = 'tcp'
        if isUDP:
	    udp = 'udp'
	
	msg = str(self.userClient.clientID)+'%'+str(reciever[0])+'%'+filename+'%'+str(os.path.getsize(path))+'%'+ udp +'<!~`~!>'
	string = f.read(1024)
        while string!="":
            msg += string
            string = f.read(1024)
        f.close()
        
        msg = encrypt(msg)
        sent = 0
        while(sent<len(msg)):
            if len(msg)<(sent+1024):
                s.send(msg[sent:sent+1024])
                #s.send(encrypt(msg[sent:sent+1024]))
                sent += 1024
            else:
                s.send(msg[sent:])
                #s.send(encrypt(msg[sent:]))
                break
        s.close()
        
    
    def clientForClientID(self,clientID):
	for client in self.clientsList:
	    if(int(clientID)==int(client[0])):
		return client
	return None
	
    def roomForRoomID(self,roomID):
	for room in self.chatRoomsList:
	    if(int(roomID)==int(room[0])):
		return room
	return None
	
    def sendToServer(self,message):
	if(message==None or message==''):
	    return
        # Send the data
        try:
	    len_sent = self.serverSocket.send(encrypt(message))
	    #if (self.guiSingleton!=None):
		#update UI with message received from the server on the main UI thread
		#wx.CallAfter(self.guiSingleton.recieved, response)
	except	socket.error, e:
	    print e
	        
    def closeConnection(self):
        # Clean up
        self.serverSocket.shutdown(socket.SHUT_RDWR)
        self.serverSocket.close()
        
    def deleteChatRoom(self,roomID):
	if(self.userCR.roomID==int(roomID)):
	    #remove urself from the chatroom 
	    pass
	self.clientsList.remove(self.roomForRoomID(roomID))
	wx.CallAfter(self.guiSingleton.updateChatRoomsList, self.chatRoomsList)
	
    def request_clients(self,clients):
        usrID ,sep, rem = clients.partition('<!~`~!>')
        usrn ,sep, rem = rem.partition('<!~`~!>')
        self.clientsList = []
        while (usrn and usrID ):
            self.clientsList.append((usrID,usrn))
            usrID ,sep, rem = rem.partition('<!~`~!>')
            usrn ,sep, rem = rem.partition('<!~`~!>')
        wx.CallAfter(self.guiSingleton.updateClientsList, self.clientsList)
        
    def request_rooms(self,rooms):
        roomID ,sep, rem = rooms.partition('<!~`~!>')
        roomName ,sep, rem = rem.partition('<!~`~!>')
        while (roomName and roomID):
            self.chatRoomsList.append((roomID,roomName))
            roomID ,sep, rem = rem.partition('<!~`~!>')
            roomName ,sep, rem = rem.partition('<!~`~!>')
	wx.CallAfter(self.guiSingleton.updateChatRoomsList, self.chatRoomsList)

            
    def connect_to_server(self,ip,username):
        self.sendToServer('#$CONN%'+ip+'%'+username+'$$#')
        
    def disconnect(self):
        self.sendToServer('#$DISCONN%'+str(self.userClient.clientID)+'$$#')

    def create_CR(self,private):
        s = ''
        if private:
            s = 'private'
        else:
            s = 'public'
        self.sendToServer('#$CREATE_CR%'+str(self.userClient.clientID)+'%'+s+'$$#')
        
    def sendMSG_to_CR (self,MSG):
        self.sendToServer('#$CR_MSG%'+str(self.userCR.roomID)+'%'+str(self.userClient.clientID)+'$'+MSG+'$#')

    def invite_client(self,clientID):
        self.sendToServer('#$INVITE_CLIENT%'+str(self.userClient.clientID)+'%'+str(self.userCR.roomID)+'%'+str(clientID)+'$$#')
    
    def accept_invitation(self,CRID):
        self.sendToServer('#$ACC_INVIT%'+str(self.userClient.clientID)+'%'+str(CRID)+'$$#')

    def join_public_CR(self,CRID):
        self.sendToServer('#$JOIN_CR%'+str(self.userClient.clientID)+'%'+str(CRID)+'$$#')
        #add CR to user's chat rooms
        
    def send_clients_request(self,CRID):
	self.sendToServer('#$REQ_CLIENTS%'+str(self.userClient.clientID)+'%'+str(CRID)+'$$#')

    def send_chatrooms_request(self):
	self.sendToServer('#$REQUEST_CRS%'+str(self.userClient.clientID)+'$$#')
    
    def msgRecieved(self, clientID, roomID, msg):
	if(self.userCR.roomID==int(roomID)):
	    client = self.clientForClientID(clientID)
	    if client:
	        wx.CallAfter(self.guiSingleton.msgRecieved, client[1]+': '+msg)
	    elif int(clientID) == self.server:
		wx.CallAfter(self.guiSingleton.msgRecieved, 'Server'+': '+msg)
	    
    def inviteLobbyToPrivateRoom(self, clients):
	for client in clients:
	    invite_client(client)
	    
    def logout_to_lobby(self):
        self.sendToServer('#$LOG_OUT%'+str(self.userClient.clientID)+'%'+str(self.userCR.roomID)+'$$#')
        
    def kick(self,kickedID):
        self.sendToServer('#$KICK%'+str(self.userClient.clientID)+'%'+str(kickedID)+'%'+str(self.userCR.roomID)+'$$#')
        
    def mute(self,mutedID):
        self.sendToServer('#$MUTE%'+str(self.userClient.clientID)+'%'+str(mutedID)+'%'+str(self.userCR.roomID)+'$$#')

    def unmute(self,mutedID):
        self.sendToServer('#$UNMUTE%'+str(self.userClient.clientID)+'%'+str(mutedID)+'%'+str(self.userCR.roomID)+'$$#')


''' end client '''

# connection ack
# chat room msg
# send chat room msg
# recieve active clients
# recieve public chat rooms


# added to chat room ack with clientID and roomID
