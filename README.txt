codelicious-im: a GUI Instant Messaging application using python
version: 0.0.1

* Requirements:
---------------
Python >= 2.3
wxPython >= 2.6


* Execution:
---------------
To start the server application, reach for ChatEngine folder,  enter ''python ChatServer.py'' in your shell,
and to start the client application, enter ''python ClientFrame.py'' in your shell.
You have to enter a username in order to start the application but this username shouldn't be used by someone already connected to the application.
You can make you own chat room from clicking on 'Lobby' in the menu bar and then choosing whether to make your room public or private.
You can also stay in the lobby and chat with everyone who hasn't joined a chat room yet.


* Documentation:
----------------
There's a documentation to the code in the file 'Report.pdf'.

* Bugs:
-------
If the server is closed while clients are connected to it, 
client is not updated with this information and some exceptions can not be handled yet on the client's side.
It's not advisable to do this at least for now, but in the future it will be handled when peer-to-peer is added.
Most of the functionality of the IM is implemented in the Back end (Engine) but isn't linked yet to the UI due to shortage in time.

For any kind of question, here are our mails that you can reach us,
	ahmad.soliman@student.guc.edu.eg
	malatawy15@gmail.com
	
Enjoy!
CoDelicious
