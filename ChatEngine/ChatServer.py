'''
Created on Oct 8, 2012

@author: el-doc
'''

import socket
from thread import *
import time
from socket import errno
from collections import deque

def encrypt(msg):
    key = 'mY_sEcReT_kEy'
    encryped = []
    for i, c in enumerate(msg):
        key_c = ord(key[i % len(key)])
        msg_c = ord(c)
        encryped.append(chr((msg_c + key_c) % 127))
    return ''.join(encryped)

def decrypt(encryped):
    key = 'mY_sEcReT_kEy'
    msg = []
    for i, c in enumerate(encryped):
        key_c = ord(key[i % len(key)])
        enc_c = ord(c)
        msg.append(chr((enc_c - key_c) % 127))
    return ''.join(msg)

class Client:
    clientID=None
    ip=None
    conn=None
    username=None
    enrolledChatRooms=[]
    mute = False
    def __init__(self, clientID, ip, conn, username):
	self.ip = ip
	self.conn = conn
	self.clientID = clientID
	self.username = username
	self.mute = False

class ChatRoom:
    server=None #server as a client
    roomID=None #String
    admin=None #Client
    private=False #Boolean
    activeClients=None
    chatRoom_name= None
    def __init__(self, server, roomID, admin, private, name):
	self.server = server
	self.roomID = roomID
	self.admin = admin
	self.private = private
	self.activeClients = []
	self.activeClients.append(admin)
	self.chatRoom_name = name
	admin.enrolledChatRooms.append(self)
	msg = '#$ADD_SUCC$'+name+'%'+str(roomID)+'$#'
	self.sendMsgToClient(admin,msg)
	self.spreadMsg(self.server.clientID, '#$CR_MSG%'+str(roomID)+'%'+str(self.server.clientID)+'$ChatRoom instantiated.$#')
	self.spreadMsg(self.server.clientID,'#$NEW_ROOM$$#')

    def destroy(self, clientID):
	if(admin.clientID==int(clientID)):
	    self.spreadMsg(self.server.clientID, '#DEL_CR%'+str(roomID)+'$$#')
    
    def addClient(self, newClient):
        print 'ADDING CLIENT: ',newClient.clientID,' TO: ',self.roomID
	for client in self.activeClients:
	    if client.clientID == newClient.clientID:
		return
	self.activeClients.append(newClient)
	newClient.enrolledChatRooms.append(self)
	msg = '#$ADD_SUCC$'+self.chatRoom_name+'%'+str(self.roomID)+'$#'
	self.sendMsgToClient(newClient,msg)
	#inform Clients
	self.spreadMsg(self.server.clientID, '#$ADD_CLIENT%'+str(self.roomID)+'$'+str(newClient.clientID)+'<!~`~!>'+newClient.username+'$#')

    def removeClient(self, client):
	if self.activeClients.count(client):
	    self.activeClients.remove(client)
	client.enrolledChatRooms.remove(self)
	#inform Clients	print 'error here'
	self.spreadMsg(self.server.clientID, '#$DEL_CLIENT%'+str(self.roomID)+'%'+str(client.clientID)+'$$#')

    def spreadMsg(self,fromClientID, msg):
	for client in self.activeClients:
	    if client != self.server and client.clientID != int(fromClientID):
		start_new_thread(self.sendMsgToClient,(client,msg))
	    
    def sendMsgToClient(self, client, msg):
	if not client:
            return
    	if(msg==None or msg==''):
	    return
        # Send the data
        try:
	    if client.conn:
		client.conn.send(encrypt(msg))
	except	socket.error, e:
	    print e

class ChatServer:
    clientsList=None 	# connected clients to server
    chatRooms=None	# running chat rooms
    processQueue=None	# process queue for handling clients requests
    
    initialClientId = 1000	# id counter for clients
    serverAsClient=Client(initialClientId, None, None, 'Server')	# server saved as a client
    intialChatRoomID=100	# id counter for chat rooms
    lobby= ChatRoom(serverAsClient, intialChatRoomID, serverAsClient, False, 'Lobby')		#lobby saved as a chatroom

    def main(self):
	self.clientsList=[]
	self.chatRooms=[]
	self.processQueue=deque([])
	
        address = ('localhost', 4321)
        serverSocket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        serverSocket.bind(address)
        serverSocket.listen(100)
        
        start_new_thread(self.dataProcessor,())
	
	start_new_thread(self.listenToFileTransfers,())
	
	while (True):
            conn,clientAddress = serverSocket.accept()
            start_new_thread(self.connectClient,(conn,clientAddress[0],))
	serverSocket.close()	
        
    def listenToFileTransfers(self):
	address = ('localhost', 8888)
        filesSocket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        #filesSocketUDP = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
        filesSocket.bind(address)
        filesSocket.listen(100)
        
        while (True):
            conn,clientAddress = filesSocket.accept()
            start_new_thread(self.recieveFile,(conn,clientAddress[0],))
        filesSocket.shutdown(socket.SHUT_RDWR)
	filesSocket.close()
	
    def recieveFile(self, (conn),clientAddress):
	dataEncr = conn.recv(1024)
        while True:
            data = conn.recv(1024)
            if not data:
                break
            dataEncr += data
	conn.shutdown(socket.SHUT_RDWR)
        conn.close()
        	
	data = decrypt(dataEncr)
	recieverData , sep, fileString = data.partition('<!~`~!>')
	senderID, sep, rest = data.partition('%')
	recieverID, sep, rest = rest.partition('%')
	fileName, sep, rest = rest.partition('%')
	fileSize, sep, rest = rest.partition('%')
	isUDP, sep, rest = rest.partition('%')
		
	#send file
	reciever = self.clientForClientID(recieverID)
	
	s = None
        if(isUDP=='udp'):
	    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        else:
	    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect(('localhost', (8000+int(recieverID)))) #self.reciever.ip
          
        time.sleep(2.0)
	s.send(encrypt(recieverData+'<!~`~!>'))
	time.sleep(2.0)
	
	fileString = encrypt(fileString)
	
	sent = 0
	while(sent<len(fileString)):
	    if len(fileString)<(sent+1024):
		s.send(fileString[sent:sent+1024])
		sent += 1024
	    else:
                s.send(fileString[sent:])
		break
	s.shutdown(socket.SHUT_RDWR)
        s.close()
	
    def connectClient(self,(conn),clientAddress):
	client = Client(-1,clientAddress,conn,'')
	self.clientsList.append(client)
        while True:
            data = conn.recv(4096)
            data = decrypt(data)
            self.processQueue.append(data)
            if not data:
                break
	conn.shutdown(socket.SHUT_RDWR)
	conn.close()

	client.conn = None
	time.sleep(300.0)
	if not client.conn:
	  if (self.clientsList.count(client)):
	    self.clientsList.remove(client)
    
    def dataProcessor(self):
	while True:
	    if self.processQueue:
		data = self.processQueue.popleft()
		print data
		if not data:
		    continue
		x,sep,z = data.partition('$')

		CMD, sep, MSGRest = z.partition('$')

		MSG=''
		if MSGRest[-1]=='#' and MSGRest[-2]=='$':
		    MSG = MSGRest[0:-2]
		else:
		    continue

		self.analyzeCMD(CMD, MSG)
	    else:
		time.sleep(1)
	return

    def analyzeCMD(self,CMD, MSG):
	
	cmdName,sep,rest = CMD.partition('%')
	if cmdName=='CONN':
	    ip, sep, name = rest.partition('%')
	    self.registerClient(ip,name)
	elif cmdName=='DISCONN':
	    self.removeClient(int(rest))
	elif cmdName=='CREATE_CR':
	    clientID, sep, rest = rest.partition('%')
	    roomType, sep, rest = rest.partition('%')
	    self.createChatRoom(clientID, roomType)
	elif cmdName=='DEL_CR':
	    CRID, sep, rest = rest.partition('%')
            clientID, sep, rest = rest.partition('%')
	    self.deleteChatRoom(CRID, clientID)
	elif cmdName=='CR_MSG':
            roomID, sep, rest = rest.partition('%')
            clientID, sep, rest = rest.partition('%')
            self.sendMsgToChatRoom(clientID, roomID, MSG)
	elif cmdName=='REQUEST_CRS':
	    clientID, sep, rest = rest.partition('%')
	    self.sendChatRooms(clientID)
	elif cmdName=='REQ_CLIENTS':
            clientID, sep, chatRoomID = rest.partition('%')
            self.clients_in_CR(int(clientID),int(chatRoomID))
	elif cmdName=='JOIN_CR':
	    clientID, sep, chatRoomID = rest.partition('%')
	    self.joinCR(int(clientID), int (chatRoomID))
	elif cmdName=='INVITE_CLIENT':
	    senderID, sep, rest = rest.partition('%')
	    chatRoomID,sep,clientID = rest.partition('%')
	    self.inviteClient(int (senderID), int(clientID), int (chatRoomID))
	elif cmdName == 'RECONN':
	    ip, sep, identity = rest.partition('%')
	    self.reconnectClient(int(identity),ip)
	elif cmdName == 'ACC_INVIT':
	    # malatawyyyyyyyyyy
            clientID, sep, CRID = rest.partition('%')
            self.reconnectClient(int(clientID),int(CRID))
	elif cmdName == 'LOG_OUT':
            clientID, sep, CRID = rest.partition('%')
            self.logout(clientID,CRID)
	elif cmdName == 'MUTE':
		adID, sep, rest = rest.partition('%')
		mID,sep,CRID = rest.partition('%')
		self.mute_client(adID,mID,CRID)
	elif cmdName == 'UNMUTE':
		adID, sep, rest = rest.partition('%')
		mID,sep,CRID = rest.partition('%')
		self.unmute_client(adID,mID,CRID)
	elif cmdName == 'KICK':
		adID, sep, rest = rest.partition('%')
		kID,sep,CRID = rest.partition('%')
		self.kick(adID,kID,CRID)
	else:
	    print 'error in CMD: ' , CMD ,' ,msg: ', MSG

    def sendMsgToClient(self,clientID, msg):
	client = self.clientForClientID(clientID)
	if client:
	    client.conn.send(encrypt(msg))
	    
    def clientForClientID(self,clientID):
	for client in self.clientsList:
	    if(int(clientID)==client.clientID):
		return client
	return None

    def chatroomForCRID(self,CRID):
	for room in self.chatRooms:
	    if(int(CRID)==room.roomID):
		return room
	return None
	
    def createChatRoom(self,clientID, roomType):
	private = False
	if roomType=='private':
	    private = True
	elif roomType=='public':
	    pass
	else:
	    #inform user that chatroom was not created
	    return
	self.intialChatRoomID +=1
	client = self.clientForClientID(clientID)
	if client:
	    chatroom = ChatRoom(self.serverAsClient,self.intialChatRoomID,client,private, client.username+'\'s Chat Room')
	    self.chatRooms.append(chatroom)
	    
	# inform lobby and clients in lobby with the new chat room 		malatawyyyyyyyyyy
    
    def deleteChatRoom(self, CRID, userID):
	room = chatroomForCRID(CRID)
	room.destroy(userID)
	chatRooms.remove(room)
    
    def registerClient(self,ip, name):
	if not ip or ip == 'localhost':
	    ip = '127.0.0.1'
	for client in self.clientsList:
	    if client.username == name:
		self.sendMsgToClient(client.clientID,'#$CONN_FAILED$$#')
		return
	for client in self.clientsList:
	    if client.clientID == -1 and client.ip==ip:
		client.username = name
		self.initialClientId +=1
		client.clientID = self.initialClientId
		#send ACK to user
		self.sendMsgToClient(client.clientID,'#$USER_CONF$'+name+'%'+str(client.clientID)+'$#')
		self.lobby.addClient(client)
		return

    def removeClient(self,ID):
	client = self.clientForClientID(ID)
	if client and self.clientsList.count(client):
	    self.clientsList.remove(client)
	else:
            print 'Not Found'
	  
    def reconnectClient(self,ID,ip):
	for client1 in self.clientsList:
	    if client1.clientID == ID:
		for client in self.clientsList:
		    if client.clientID == -1 and client.ip==ip:
			client1.conn = client.conn
			self.clientsList.remove(client)
			self.sendMsgToClient(clientID,'#$RECONN_SUCC%'+str(ID)+'$$#')
			return
	else:
	    print 'Not Found'

    def sendMsgToChatRoom(self, clientID, roomID, msg):
	room = self.chatroomForCRID(roomID)
	client = self.clientForClientID(clientID)
	if not client:
		return
	if client.mute:
		return
	if int(roomID)==self.lobby.roomID:
	    room = self.lobby
	if room:
	    room.spreadMsg(clientID,'#$CR_MSG%'+str(roomID)+'%'+str(clientID)+'$'+msg+'$#')
	    
    def inviteClient (self, senderID, clientID, chatRoomID):
	self.sendMsgToClient(clientID,'#$INVITE%'+str(senderID)+'%'+str(chatRoomID)+'$$#')
            
    def accept_invitation (self, clientID, chatRoomID):
        room = self.chatroomForCRID(chatRoomID)
        if room:
	    client = self.clientForClientID(clientID)
	    if client:
		room.addClient(client)
		return
	    else:
		print 'Client not found'
        else:
            print 'Chat room not found'
            
            
    def joinCR(self,clientID, chatRoomID):
	room = self.chatroomForCRID(chatRoomID)
        if room:
	    client = self.clientForClientID(clientID)
	    if client:
		room.addClient(client)
		return
	    else:
		print 'Client not found'
        else:
            print 'Chat room not found'
            
            
    def sendChatRooms(self, clientID):
        s = '#$CRS%'+str(clientID)+'$'
        for CR in self.chatRooms:
	    if CR!=self.lobby and not CR.private:
		s += (str(CR.roomID)+'<!~`~!>`'+CR.chatRoom_name+'<!~`~!>')
        s += '$#'
        self.sendMsgToClient(clientID,s)
        
    def clients_in_CR (self, clientID, cRoomID):
        s = '#$CR_CLIENTS%'+str(cRoomID)+'$'
        room = self.chatroomForCRID(cRoomID)
        if cRoomID==100:
	    room = self.lobby
	print 'Room Active Clients: ',room.activeClients
        for client in room.activeClients:
            if client.clientID != clientID and client.clientID != self.serverAsClient.clientID:
		s += (str(client.clientID)+'<!~`~!>'+client.username+'<!~`~!>')
        s += '$#'
        print s
        self.sendMsgToClient(clientID,s)
        
    def deleteCR(self, CRID, cID):
        room = chatroomForCRID(CRID)
        if cID == room.admin.clientID:
            for client in room.activeClients:
                self.lobby.addClient(client)
        else:
            print 'Not Authorized'
        self.chatRooms.remove(room)
    
    def logout(self, clientID, cRoomID):
        room = self.chatroomForCRID(cRoomID)
        client = self.clientForClientID(clientID)
        if not room or not client:
            return
        room.activeClients.remove(client)
        self.lobby.addClient(client)
        if client.clientID == room.admin.clientID:
	    if room.activeClients:
		room.admin = room.activeClients[0]
	    else:
		self.deleteCR(room.roomID,client.clientID)
		
	def kick(self,adID,kickedID,CRID):
		cr = self.chatroomForCRID(CRID)
		if not cr:
			return
		if not cr.admin.clientID == adID:
			return
		for c in cr.activeClients:
			if c.clientID == kickedID:
				self.logout(kickedID,CRID)
				self.sendMsgToClient(c,'#$KICKED%'+str(kickedID)+'$$#')

	def mute_client(self,adID,kickedID,CRID):
		cr = self.chatroomForCRID(CRID)
		if not cr:
			return
		if not cr.admin.clientID == adID:
			return
		for c in cr.activeClients:
			if c.clientID == kickedID:
				c.mute = True

	def unmute_client(self,adID,kickedID,CRID):
		cr = self.chatroomForCRID(CRID)
		if not cr:
			return
		if not cr.admin.clientID == adID:
			return
		for c in cr.activeClients:
			if c.clientID == kickedID:
				c.mute = False

if __name__ == "__main__":
    c = ChatServer()
    c.main()
''' end server '''

#$CONN%127.0.0.1%malatawy15$$#
#$DISCONN%1001$$#
#$CREATE_CR%1001%public$$#
#$CR_MSG%101%1001$ChatRoom instantiated.$#
#$INVITE_CLIENT%1001%101%1002$$#
#$ACC_INVIT%1002%101$$#
#$JOIN_CR%1002%101$$#
